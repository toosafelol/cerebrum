from itertools import combinations

def struct():
    pass
    #return Neuron(
    #    default=Synapse(Learnnet))


stl = lambda s: s.split(" ")

def bug(s):
    print s

class Learnnet(object):
    def learn(self, subject):
        for combination in self._options(subject):
            for x in combination:
                print x

    def _options(self, subject):
        return ((x for x in combinations(stl(subject), y+1)) for y in reversed(range(len(stl(subject)))))


if __name__ == "__main__":
    from sys import argv as args
    a = " ".join(args[1:])
    Learnnet().learn(a)
