# from universe.types import Solid
from universe.molecules import sio2, ai2o3, k2o, na2o, cao, feo, fe2o3, mgo, tio2, p2o5, mno

class Granite(Solid):
    GRAMS_PER_SQUARE_CM = 2.7; 
    ATMOSPHERIC_MELTING_RANGE = r(1488.15, 1533.15)
    PRESSURIZED_MELTNG_RANGE = r(923.15)
    COMPOSITION = set(sio2, ai2o3, k2o, na2o, cao, feo, fe2o3, mgo, tio2, p2o5, mno)

